const express = require('express');
const multer = require('multer');
const path = require('path');

const router = express.Router();
const upload = multer({ dest: path.join(__dirname, '../uploads') });

router.post('/upload', upload.single('file'), function(req, res, next) {
  res.json({
    size: req.file.size
  });
});

module.exports = router;
